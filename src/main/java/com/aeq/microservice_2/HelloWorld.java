package com.aeq.microservice_2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/service_2")
public class HelloWorld {

    @Autowired
    Service1FeignClient service1FeignClient;

    @GetMapping
    public String getHelloWorld() {
        return service1FeignClient.getHelloWorld();
    }
}
